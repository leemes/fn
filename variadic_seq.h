#ifndef FN_VARIADIC_SEQ_H
#define FN_VARIADIC_SEQ_H

#include <cstdlib>

// The following implementation of index sequence is similar to the proposed types for C++14.

namespace fn {

template<std::size_t... Ints>
class index_sequence {
    static constexpr std::size_t size() noexcept { return sizeof...(Ints); }
};


template<std::size_t N, std::size_t ...S>
struct make_index_sequence_helper : make_index_sequence_helper<N-1, N-1, S...> {};
template<std::size_t ...S>
struct make_index_sequence_helper<std::size_t(0), S...> {
    using type = index_sequence<S...>;
};

template<std::size_t N>
using make_index_sequence = typename make_index_sequence_helper<N>::type;

template<typename...Args>
using index_sequence_for = make_index_sequence<sizeof...(Args)>;

}

#endif
