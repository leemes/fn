#ifndef FN_FILTER_H
#define FN_FILTER_H

#include "adaptor.h"
#include "range.h"
#include "call.h"
#include "comp.h"
#include "templated_functor.h"

namespace fn {

// forward declarations
template<class, class> class FilterAdaptor;


/** Haskell's "filter" function */
template<class R, class Fn>
inline FilterAdaptor<MakeRange<R&&>,Fn> filter(R &&range, const Fn &f) {
    return { makeRange(std::forward<R>(range)), f };
}

/** Like "filter", but the predicate gets called on a member "first" (which represents the key in an associative range). */
template<class R, class Fn>
inline auto filterKeys(R &&range, const Fn &f) -> decltype(filter(std::forward<R>(range), comp(fn::getKey, f))) {
    return filter(std::forward<R>(range), comp(fn::getKey, f));
}

/** Like "filter", but the predicate gets called on a member "second" (which represents the value in an associative range). */
template<class R, class Fn>
inline auto filterValues(R &&range, const Fn &f) -> decltype(filter(std::forward<R>(range), comp(fn::getValue, f))) {
    return filter(std::forward<R>(range), comp(fn::getValue, f));
}

/** Convenience function: mapped "filter": Filters a range according to a mapped value, but returning a range with the original (unmapped) values. Especially useful for filtering by member variables or by values as returned by "getter" member functions. In haskell, this would be: "\ range m f -> filter (f . m) range". */
template<class R, class MapFn, class FilterFn>
inline auto filter(R &&range, const MapFn &m, const FilterFn &f) -> decltype(filter(std::forward<R>(range), comp(m, f))) {
    return filter(std::forward<R>(range), comp(m, f));
}

/** Like mapped "filter", but the predicate gets called on a member "first" (which represents the key in an associative range). */
template<class R, class MapFn, class FilterFn>
inline auto filterKeys(R &&range, const MapFn &m, const FilterFn &f) -> decltype(filterKeys(std::forward<R>(range), comp(m, f))) {
    return filterKeys(std::forward<R>(range), comp(m, f));
}

/** Like mapped "filter", but the predicate gets called on a member "second" (which represents the value in an associative range). */
template<class R, class MapFn, class FilterFn>
inline auto filterValues(R &&range, const MapFn &m, const FilterFn &f) -> decltype(filterValues(std::forward<R>(range), comp(m, f))) {
    return filterValues(std::forward<R>(range), comp(m, f));
}



/** Helper class for temporary storage of the result of a filter() operation. This is required since the resulting container isn't known yet when filter() is called. */
template<class R, class Fn>
class FilterAdaptor : public detail::Adaptor<FilterAdaptor<R,Fn>>
{
    R range;
    Fn f;

    using R_It = Iterator<R>;

public:
    FilterAdaptor(const R &range, const Fn &f) :
        range(range), f(f)
    {
    }

    //! The adaptor can be used as a regular range by introducing this iterator class.
    class const_iterator {
        R_It original;
        R_It original_end;
        const Fn &f;

        // Advances the original iterator until the filter function returns true
        void skipFilteredOut() {
            while (original != original_end && !bool(call(f,*original)))
                ++original;
        }

    public:
        const_iterator (const R_It &original, const R_It &original_end, const Fn &f) :
            original(original), original_end(original_end), f(f)
        { skipFilteredOut(); }

        // forward iterator implementation
        decltype(*original) operator*() const { return *original; }
        decltype(original) operator->() const { return original; }
        const_iterator& operator++() { ++original; skipFilteredOut(); return *this; }
        const_iterator operator++(int) { const_iterator old(*this); ++(*this); return old; }
        bool operator==(const const_iterator & o) const { return original == o.original; }
        bool operator!=(const const_iterator & o) const { return original != o.original; }

        // iterator traits
        using iterator_category = std::forward_iterator_tag;
        using value_type        = const typename std::decay<decltype(*std::declval<const_iterator>())>::type;
        using difference_type   = std::size_t;
        using pointer           = value_type*;
        using reference         = value_type&;
    };
    const_iterator begin() const { return const_iterator(std::begin(range), std::end(range), f); }
    const_iterator end() const { return const_iterator(std::end(range), std::end(range), f); }

    using iterator = const_iterator;
    using value_type = typename const_iterator::value_type;
};


}

#endif // FN_FILTER_H
