#ifndef FN_COMBINE_H
#define FN_COMBINE_H

#include "range.h"
#include "adaptor.h"
#include "call.h"
#include "variadic_ops.h"
#include "variadic_seq.h"

#include <tuple>

namespace fn {

// forward declarations
template<class ...> class CombineAdaptor;



/** Returns a lazy range representing all tuples constructed from any elements from multiple source ranges. */
template<class ...Rs>
CombineAdaptor<MakeRange<Rs&&>...> combine(Rs &&...ranges) {
    return { makeRange(std::forward<Rs>(ranges))... };
}





template<class ...Rs>
class CombineAdaptor : public detail::Adaptor<CombineAdaptor<Rs...>>
{
    std::tuple<Rs...> ranges;

    using Iseq = index_sequence_for<Rs...>;

public:
    CombineAdaptor(const Rs &...ranges) :
        ranges(std::make_tuple(ranges...))
    {}

    //! The adaptor can be nested by introducing this iterator class.
    class const_iterator {
        std::tuple<Iterator<Rs>...> original_curr;
        std::tuple<Iterator<Rs>...> original_begin;
        std::tuple<Iterator<Rs>...> original_end;

        // Variadic tuple helpers:
        template<std::size_t ...Is>
        auto deref(index_sequence<Is...>) const -> decltype(std::make_tuple(*std::get<Is>(original_curr)...)) {
            return std::make_tuple(*std::get<Is>(original_curr)...);
        }
        template<std::size_t ...Is>
        void advance(index_sequence<Is...>) {
            // advance the iterators to their next combination; if this overflows (true will be returned), they have been put to begin, so we put them to the end.
            if (va_next_combination(std::tie(std::get<Is>(original_curr), std::get<Is>(original_begin), std::get<Is>(original_end))...)) {
                va_noop(std::get<Is>(original_curr) = std::get<Is>(original_end)...);
            }
        }
        template<std::size_t ...Is>
        bool all_iters_equal_in(index_sequence<Is...>, const const_iterator & o) const {
            return va_all(std::get<Is>(original_curr) == std::get<Is>(o.original_curr)...);
        }

    public:
        const_iterator(const std::tuple<Iterator<Rs>...> & original_curr,
                       const std::tuple<Iterator<Rs>...> & original_begin,
                       const std::tuple<Iterator<Rs>...> & original_end) :
            original_curr(original_curr), original_begin(original_begin), original_end(original_end)
        {}

        auto operator*() const -> decltype(this->deref(Iseq())) { return this->deref(Iseq()); }
        const_iterator & operator++() { advance(Iseq()); return *this; }
        const_iterator operator++(int) { const_iterator old(*this); ++(*this); return old; }
        bool operator==(const const_iterator &o) const { return all_iters_equal_in(Iseq(), o); }
        bool operator!=(const const_iterator &o) const { return !(*this == o); }

        // iterator traits
        using iterator_category = std::forward_iterator_tag;
        using value_type        = typename std::decay<decltype(*std::declval<const_iterator>())>::type;
        using difference_type   = std::size_t;
        using pointer           = const value_type*;
        using reference         = const value_type&;
    };
    const_iterator begin() const { return { get_begin_tuple(Iseq()), get_begin_tuple(Iseq()), get_end_tuple(Iseq()) }; }
    const_iterator end()   const { return { get_end_tuple(Iseq()),   get_begin_tuple(Iseq()), get_end_tuple(Iseq()) }; }

    using iterator = const_iterator;
    using value_type = typename const_iterator::value_type;

private:
    // Variadic tuple helpers:
    template<std::size_t ...Is>
    std::tuple<Iterator<Rs>...> get_begin_tuple(index_sequence<Is...>) const {
        // If any of the ranges is empty, we need to return the end iterator immediately.
        // Otherwise the iteration is broken.
        if (va_any(fn::empty(std::get<Is>(ranges))...))
            return std::make_tuple(std::get<Is>(ranges).end()...);
        else
            return std::make_tuple(std::get<Is>(ranges).begin()...);
    }
    template<std::size_t ...Is>
    std::tuple<Iterator<Rs>...> get_end_tuple(index_sequence<Is...>) const {
        return std::make_tuple(std::get<Is>(ranges).end()...);
    }
};


}

#endif // FN_COMBINE_H
