#ifndef FN_FN_H
#define FN_FN_H

#include <functional>

#include "range.h"
#include "call.h"
#include "comp.h"
#include "fold.h"
#include "filter.h"
#include "map.h"
#include "zipwith.h"
#include "arithmetic.h"
#include "iterate.h"
#include "combine.h"

#endif // FN_FN_H
