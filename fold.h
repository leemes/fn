#ifndef FN_FOLD_H
#define FN_FOLD_H

#include "range.h"
#include <cassert>

namespace fn {

// foldl
template<class R, class Fn, class Init, class T = typename std::decay<Init>::type>
T foldl(R &&range, Init &&initial, Fn &&f) {
    T val = initial;
    for (const T & next : range) {
        const T & curr = val; // forbid f to use T& as the first parameter type
        val = f(curr, next); // If you get a compilation error here, you most probably used fold with a function taking a T& as its first or second parameter, which is forbidden.
    }
    return val;
}

// foldl without initial element (foldl1 in haskell)
template<class R, class Fn, class T = ValueType<R>>
inline T foldl(R &&range, Fn &&f) {
    using std::begin;
    assert(!empty(range)); // the provided range has to be non-empty!
    return foldl(tail(range), *begin(range), f);
}

// foldr
template<class R, class Fn, class Init, class T = typename std::decay<Init>::type>
T foldr(R &&range, Init &&initial, Fn &&f) {
    return foldl(reverse(std::forward<R>(range)), initial, [&f](T a, T b){ return f(b, a); });
}

// foldr without initial element (foldr1 in haskell)
template<class R, class Fn, class T = ValueType<R>>
inline T foldr(R &&range, Fn &&f) {
    return foldl(reverse(std::forward<R>(range)), [&f](T a, T b){ return f(b, a); });
}

}

#endif // FN_FOLD_H
