#ifndef FN_ADAPTOR_H
#define FN_ADAPTOR_H

#include "range.h"


namespace fn {

namespace detail {

template<typename A>
class Adaptor {
    // Lifting the "this" pointer to the sub-type A provided with CRTP
    const A* that() const { return static_cast<const A*>(this); }

public:
    //! Converting any adaptor to a container will call convert() on this adaptor.
    template<class ResultingContainer>
    operator ResultingContainer() const {
        return convert<ResultingContainer>(*that());
    }

    //! Calling ".into(target)" on any adaptor will call into() on the given container and this adaptor, effectively adding the adaptor's result into the container.
    template<class TargetContainer>
    void into(TargetContainer & target) const {
        fn::into(target, *that());
    }
};


template<typename R_It>
struct AdaptorIterator {
    auto operator->() -> decltype(&**this) {
        return &**this;
    }
};


}

}

#endif
