#ifndef FN_VARIADIC_REDUCE_H
#define FN_VARIADIC_REDUCE_H

#include <tuple>


namespace fn {


//! Dummy variadic function call, useful to expand parameter packs for "just expressions", e.g. incrementing iterators.
template <typename ...Ts>
inline void va_noop(Ts &&...) {
}


//! Returns true iif all arguments, converted to bool, are true.
inline bool va_all() {
    return true;
}
template <typename Head, typename ...Tail>
inline bool va_all(Head &&h, Tail &&...t) {
    return static_cast<bool>(h) && va_all(t...);
}

//! Returns true iif any argument, converted to bool, is true.
inline bool va_any() {
    return false;
}
template <typename Head, typename ...Tail>
inline bool va_any(Head &&h, Tail &&...t) {
    return static_cast<bool>(h) || va_any(t...);
}

//! Advances a set of iterators to the next combination. The first iterator is considered to be the "most significant" one.
//! For each iterator, also the begin and end iterator of the range has to be provided (together in a tuple of three references to iterators, i.e. a std::tie).
//! The return type indicates an "overflow" of the most significant iterator, i.e. when it reaches the end iterator.
//! This flag is used to advance the next significant iterator in the set.
inline bool va_next_combination() {
    return true;
}
template<typename Head, typename ...Tail>
inline bool va_next_combination(std::tuple<Head&,Head&,Head&> && curr_and_begin_and_end, std::tuple<Tail&,Tail&,Tail&> &&...t) {
    // advance the "tail" to its next combination and check if it had an overflow
    if (va_next_combination(std::forward<std::tuple<Tail&,Tail&,Tail&>>(t)...)) {
        // advance the "head" iterator
        ++std::get<0>(curr_and_begin_and_end);
        // check if the "head" just overflow
        bool at_end = (std::get<0>(curr_and_begin_and_end) == std::get<2>(curr_and_begin_and_end));
        // if it did, put it back to the beginning and report the overflow
        if (at_end) std::get<0>(curr_and_begin_and_end) = std::get<1>(curr_and_begin_and_end);
        return at_end;
    } else {
        // "tail" didn't overflow, so we do nothing and no overflow should be reported
        return false;
    }
}

}

#endif
