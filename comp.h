#ifndef FN_COMP_H
#define FN_COMP_H

#include "call.h"

namespace fn {

template<class Fn1, class Fn2>
class CompFn {
    Fn1 f1;
    Fn2 f2;

public:
    CompFn(const Fn1 &f1, const Fn2 &f2) : f1(f1), f2(f2) {}

    template<class ...Args> inline
    auto operator()(Args &&...args) const -> decltype(call(std::declval<Fn2>(), call(std::declval<Fn1>(), std::forward<Args>(args)...))) {
        return call(f2, call(f1, std::forward<Args>(args)...));
    }
};

/** Returns a functor which first applies f1, then f2 on the argument when being called. This is the composition of f2 after f1. Note: This is the other way around compared to Haskell's composition. */
template<class Fn1, class Fn2>
CompFn<Fn1,Fn2> comp(const Fn1 &f1, const Fn2 &f2) {
    return CompFn<Fn1,Fn2>(f1, f2);
}



}

#endif // FN_COMP_H
