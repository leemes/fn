#ifndef FN_MAP_H
#define FN_MAP_H

#include "range.h"
#include "adaptor.h"
#include "call.h"

namespace fn {

// forward declarations
template<class, class> class MapAdaptor;


/** Haskell's "map" function */
template<class R, class Fn>
inline MapAdaptor<MakeRange<R&&>,Fn> map(R &&range, const Fn &f) {
    return { makeRange(std::forward<R>(range)), f };
}

/** Like "map", but the map function gets called on a member "first" (which represents the key in an associative range). */
template<class R, class Fn, template<class,class> class PairTplType = std::pair,
         class T = ValueType<MakeRange<R&&>>,
         class TxFst = typename std::decay<decltype(call(std::declval<Fn>(),std::declval<T>().first))>::type,
         class TxSnd = typename std::decay<decltype(std::declval<T>().second)>::type,
         class Tx = PairTplType<TxFst, TxSnd>,
         class Fnx = std::function<Tx(T)>>
inline MapAdaptor<MakeRange<R&&>,Fnx> mapKeys(R &&range, const Fn &f)
{
    Fnx fx = [f](const T &t) -> Tx { return Tx{ call(f,t.first), t.second }; };
    return { makeRange(std::forward<R>(range)), fx };
}

/** Like "map", but the map function gets called on a member "second" (which represents the value in an associative range). */
template<class R, class Fn, template<class,class> class PairTplType = std::pair,
         class T = ValueType<MakeRange<R&&>>,
         class TxFst = typename std::decay<decltype(std::declval<T>().first)>::type,
         class TxSnd = typename std::decay<decltype(call(std::declval<Fn>(),std::declval<T>().second))>::type,
         class Tx = PairTplType<TxFst, TxSnd>,
         class Fnx = std::function<Tx(T)>>
inline MapAdaptor<MakeRange<R&&>,Fnx> mapValues(R &&range, const Fn &f)
{
    Fnx fx = [f](const T &t) -> Tx { return Tx{ t.first, call(f,t.second) }; };
    return { makeRange(std::forward<R>(range)), fx };
}


template<class T>
class DerefProxy {
    T t;
public:
    DerefProxy(T &&t) : t(t) {}
    const T* operator->() const { return &t; }
};

/** Helper class for temporary storage of the result of a map() operation. This is required since the resulting container isn't known yet when map() is called. */
template<class R, class Fn>
class MapAdaptor : public detail::Adaptor<MapAdaptor<R,Fn>>
{
    R range;
    Fn f;

    using R_It = typename range_traits<R>::iterator;

public:
    MapAdaptor(const R &range, const Fn &f) :
        range(range), f(f)
    {
    }

    //! The adaptor can be used as a regular range by introducing this iterator class.
    class const_iterator {
        R_It original;
        const Fn &f;

    public:
        const_iterator (const R_It &original, const Fn &f) :
            original(original), f(f)
        {
        }

        auto operator *() const -> decltype(call(f,*original)) { return call(f,*original); }
        DerefProxy<decltype(*std::declval<const_iterator>())> operator->() const { return {**this}; }
        const_iterator & operator ++() { ++original; return *this; }
        const_iterator operator ++(int) { const_iterator old(*this); ++(*this); return old; }
        bool operator ==(const const_iterator &o) const { return original == o.original; }
        bool operator !=(const const_iterator &o) const { return original != o.original; }

        // iterator traits
        using iterator_category = std::forward_iterator_tag;
        using value_type        = typename std::decay<decltype(*std::declval<const_iterator>())>::type;
        using difference_type   = std::size_t;
        using pointer           = const value_type*;
        using reference         = const value_type&;
    };
    const_iterator begin() const { return const_iterator(fn::begin(range), f); }
    const_iterator end() const { return const_iterator(fn::end(range), f); }

    using iterator = const_iterator;
    using value_type = typename const_iterator::value_type;
};


}

#endif // FN_MAP_H
