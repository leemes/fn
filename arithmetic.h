#ifndef FN_ARITHMETIC_H
#define FN_ARITHMETIC_H

#include "fold.h"
#include "range.h"
#include "traits.h"
#include "templated_functor.h"
#include <functional>

namespace fn {


#define FN_BINARY_OPERATOR_BIND1ST(name, op) \
    FN_TEMPLATED_FUNCTOR_BIND_1_CALL_1(Functor_##name, (b1 op c1)) \
    template<class T> inline Functor_##name<T> name(const T &first) { return Functor_##name<T>(first); }

#define FN_BINARY_OPERATOR_BIND2ND(name, op) \
    FN_TEMPLATED_FUNCTOR_BIND_1_CALL_1(Functor_##name, (c1 op b1)) \
    template<class T> inline Functor_##name<T> name(const T &second) { return Functor_##name<T>(second); }

#define FN_UNARY_OPERATOR(name, op) \
    FN_TEMPLATED_FUNCTOR_BIND_0_CALL_1(Functor_##name, op c1) \
    static const constexpr Functor_##name name;

// Unary (prefix) operators, binding no parameters
FN_UNARY_OPERATOR(identity, )
FN_UNARY_OPERATOR(negate, -)
FN_UNARY_OPERATOR(logicalNot, !)
FN_UNARY_OPERATOR(bitwiseNot, ~)
FN_UNARY_OPERATOR(dereference, *)
FN_UNARY_OPERATOR(reference, &)
FN_UNARY_OPERATOR(increment, ++)
FN_UNARY_OPERATOR(decrement, --)

// Arithmetic binary operators, binding the second parameter
FN_BINARY_OPERATOR_BIND2ND(plus, +)
FN_BINARY_OPERATOR_BIND2ND(minus, -)
FN_BINARY_OPERATOR_BIND2ND(multiplies, *)
FN_BINARY_OPERATOR_BIND2ND(divides, /)
FN_BINARY_OPERATOR_BIND2ND(modulus, %)

// Arithmetic binary operators, binding the first parameter
FN_BINARY_OPERATOR_BIND1ST(addedTo, +)
FN_BINARY_OPERATOR_BIND1ST(subtractedFrom, -)
FN_BINARY_OPERATOR_BIND1ST(multipliedTo, *)
FN_BINARY_OPERATOR_BIND1ST(dividedFrom, /)
FN_BINARY_OPERATOR_BIND1ST(modulusFrom, %)

// Comparison operators, binding the second parameter
FN_BINARY_OPERATOR_BIND2ND(equalTo, ==)
FN_BINARY_OPERATOR_BIND2ND(notEqualTo, !=)
FN_BINARY_OPERATOR_BIND2ND(greater, >)
FN_BINARY_OPERATOR_BIND2ND(less, <)
FN_BINARY_OPERATOR_BIND2ND(greaterEqual, >=)
FN_BINARY_OPERATOR_BIND2ND(lessEqual, <=)

// Pseudo comparison operator, returning always the provided constant boolean value
FN_TEMPLATED_FUNCTOR_BIND_1_CALL_1(Functor_always, b1)
template<class T> inline Functor_always<T> always(const T &returnValue) { return Functor_always<T>(returnValue); }


// Test for divisibility -- This is a bit more complex, so we can't use the macros from above
FN_TEMPLATED_FUNCTOR_BIND_1_CALL_1(Functor_divisibleBy, (c1 % b1) == B1{})
template<class T> inline Functor_divisibleBy<T> divisibleBy(const T &second) { return Functor_divisibleBy<T>(second); }
FN_TEMPLATED_FUNCTOR_BIND_1_CALL_1(Functor_indivisibleBy, (c1 % b1) != B1{})
template<class T> inline Functor_indivisibleBy<T> indivisibleBy(const T &second) { return Functor_indivisibleBy<T>(second); }




// sum
template<class R, class T = ValueType<R>> inline
T sum(const R & range) {
    return foldl(range, T(), std::plus<T>());
}

// product
template<class R, class T = ValueType<R>> inline
T product(const R & range) {
    return foldl(range, static_cast<T>(1), std::multiplies<T>());
}

// any
template<class R, class T = ValueType<R>> inline
T any(const R & range) {
    return foldl(range, static_cast<T>(false), std::logical_or<T>());
}

// all
template<class R, class T = ValueType<R>> inline
T all(const R & range) {
    return foldl(range, static_cast<T>(true), std::logical_and<T>());
}

// min
template<class R, class T = ValueType<R>> inline
T min(R &&range) {
    return foldl(range, [](const T &a, const T &b) { return std::min(a, b); });
}
template<class R, class CompFn, class T = ValueType<R>> inline
    EnableIf<callable_with<CompFn, T, T>::value,
T> min(R &&range, CompFn &&lessThan) {
    return foldl(range, [&](const T &a, const T &b) { return std::min(a, b, lessThan); });
}
template<class R, class MapFn, class T = ValueType<R>> inline
    EnableIf<callable_with<MapFn, T>::value,
T> min(R &&range, MapFn &&mapFunction) {
    return min(range, [&](const T &a, const T &b) {
        return bool(call(mapFunction,a) < call(mapFunction,b));
    });
}

// max
template<class R, class T = ValueType<R>> inline
T max(R &&range) {
    return foldl(range, [](const T &a, const T &b) { return std::max(a, b); });
}
template<class R, class CompFn, class T = ValueType<R>> inline
    EnableIf<callable_with<CompFn, T, T>::value,
T> max(R &&range, CompFn &&lessThan) {
    return foldl(range, [&](const T &a, const T &b) { return std::max(a, b, lessThan); });
}
template<class R, class MapFn, class T = ValueType<R>> inline
    EnableIf<callable_with<MapFn, T>::value,
T> max(R &&range, MapFn &&mapFunction) {
    return max(range, [&](const T &a, const T &b) {
        return bool(call(mapFunction,a) < call(mapFunction,b));
    });
}


// join = sum with separator
template<class R, class Separator, class T = decltype(std::declval<ValueType<R>>() + std::declval<Separator>() + std::declval<ValueType<R>>())> inline
T join(R &&range, Separator &&separator) {
    if (empty(range))
        return T{};
    return foldl(range, [&](const T &a, const T &b){
        return a + separator + b;
    });
}

}


#endif // FN_ARITHMETIC_H
