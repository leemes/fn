#ifndef FN_ZIPWITH_H
#define FN_ZIPWITH_H

#include "range.h"
#include "adaptor.h"
#include "call.h"
#include "variadic_ops.h"
#include "variadic_seq.h"

#include <tuple>

namespace fn {

// forward declarations
template<class, class...> class ZipAdaptor;


/** Haskell's "zipWith" function, but on any number of ranges. Note that in contrast to other functions in fn, this one takes the functor as its first argument, because variadic type arguments may only appear at the end of the parameter list. */
template<class ...Rs,
         class Fn,
         class ZipAdaptor = ZipAdaptor<Fn,MakeRange<Rs&&>...>>
ZipAdaptor zipWith(const Fn &zipper, Rs &&...ranges) {
    return { zipper, makeRange(std::forward<Rs>(ranges))... };
}
/** Haskell's "zip" function, but on any number of ranges, using std::make_tuple as the zipper. */
template<class ...Rs,
         class Fn = decltype(&std::make_tuple<const ValueType<MakeRange<Rs&&>>&...>),
         class ZipAdaptor = ZipAdaptor<Fn,MakeRange<Rs&&>...>>
ZipAdaptor zip(Rs &&...ranges) {
    return { &std::make_tuple<const ValueType<MakeRange<Rs&&>>&...>, makeRange(std::forward<Rs>(ranges))... };
}


/** Helper class for temporary storage of the result of a zipWith() operation. This is required since the resulting container isn't known yet when zipWith() is called. */
template<class Fn, class ...Rs>
class ZipAdaptor : public detail::Adaptor<ZipAdaptor<Fn,Rs...>>
{
    Fn f;
    std::tuple<Rs...> ranges;

    using Iseq = index_sequence_for<Rs...>;

public:
    ZipAdaptor(const Fn &f, const Rs &...ranges) :
        f(f), ranges(std::make_tuple(ranges...))
    {}

    //! The adaptor can be used as a regular range by introducing this iterator class.
    class const_iterator {
        const Fn &f;
        std::tuple<Iterator<Rs>...> original_iterators;

        // Variadic tuple helpers:
        template<std::size_t ...Is>
        auto deref(index_sequence<Is...>) const -> decltype(call(f,*std::get<Is>(original_iterators)...)) {
            return call(f,*std::get<Is>(original_iterators)...);
        }
        template<std::size_t ...Is>
        void advance(index_sequence<Is...>) {
            va_noop(++std::get<Is>(original_iterators)...);
        }
        template<std::size_t ...Is>
        bool any_iter_equal_in(index_sequence<Is...>, const const_iterator & o) const {
            return va_any(std::get<Is>(original_iterators) == std::get<Is>(o.original_iterators)...);
        }

    public:
        const_iterator(const Fn &f, Iterator<Rs> ... original_iterators) :
            f(f), original_iterators(std::make_tuple(original_iterators...))
        {}

        auto operator *() const -> decltype(this->deref(Iseq())) { return this->deref(Iseq()); }

        const_iterator & operator ++() { advance(Iseq()); return *this; }
        const_iterator operator ++(int) { const_iterator old(*this); ++(*this); return old; }

        // A zip adaptor iterator is considered equal to another, if one of the original iterators is equal. This allows zipping ranges of unequal sizes.
        bool operator ==(const const_iterator &o) const { return any_iter_equal_in(Iseq(), o); }
        bool operator !=(const const_iterator &o) const { return !(*this == o); }

        // iterator traits
        using iterator_category = std::forward_iterator_tag;
        using value_type        = typename std::decay<decltype(*std::declval<const_iterator>())>::type;
        using difference_type   = std::size_t;
        using pointer           = const value_type*;
        using reference         = const value_type&;
    };
    const_iterator begin() const { return get_begin(Iseq()); }
    const_iterator end()   const { return get_end  (Iseq()); }

    using iterator = const_iterator;
    using value_type = typename const_iterator::value_type;

private:
    // Variadic tuple helpers:
    template<std::size_t ...Is>
    const_iterator get_begin(index_sequence<Is...>) const {
        return const_iterator(f, std::get<Is>(ranges).begin()...);
    }
    template<std::size_t ...Is>
    const_iterator get_end(index_sequence<Is...>) const {
        return const_iterator(f, std::get<Is>(ranges).end()...);
    }
};

}

#endif // FN_ZIPWITH_H
