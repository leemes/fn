#ifndef FN_ITERATE_H
#define FN_ITERATE_H

#include <iterator>
#include "arithmetic.h"
#include "adaptor.h"

namespace fn {

// forward declarations
template<class, class, class> class IterateAdaptor;


/** Haskell's "iterate" function: returns an infinite range which iterates starting from an initial value using a successor function to compute the next element. */
template<class T, class CondFn = decltype(fn::always(true)), class SuccFn = decltype(fn::plus(std::declval<T>()))>
IterateAdaptor<T,CondFn,SuccFn> iterate(const T &initial, const SuccFn &successor = fn::plus(T{1})) {
    return { initial, fn::always(true), successor };
}

/** Haskell's "iterate" function with a condition: comparable to C/C++ counting for-loop. for(int i : iterateWhile(0, lessThan(100) [, plus(1)])) is equivalent to for(int i = 0; i < 100; ++i) */
template<class T, class CondFn, class SuccFn = decltype(fn::plus(1))>
IterateAdaptor<T,CondFn,SuccFn> iterateWhile(const T &initial, const CondFn &condition, const SuccFn &successor = fn::plus(T{1})) {
    return { initial, condition, successor };
}

/** Haskell's "iterate" function with a terminating value: comparable to C/C++ counting for-loop. for(int i : iterateUntil(0, 100 [, plus(1)])) is equivalent to for(int i = 0; i < 100; ++i) */
template<class T, class CondFn = decltype(fn::less(std::declval<T>())), class SuccFn = decltype(fn::plus(std::declval<T>()))>
IterateAdaptor<T,CondFn,SuccFn> iterateUntil(const T &initial, const T &end, const SuccFn &successor = fn::plus(1)) {
    assert(successor(initial) >= initial); // Only use this function with a monotonic increasing iterator
    return { initial, fn::less(end), successor };
}

/** Alias for "iterateUntil" provided for convenience. */
template<class T, class CondFn = decltype(fn::less(std::declval<T>())), class SuccFn = decltype(fn::plus(std::declval<T>()))>
IterateAdaptor<T,CondFn,SuccFn> seq(const T &initial, const T &end, const T &step = T{1}) {
    return { initial, fn::less(end), fn::plus(step) };
}


template<class T, class CondFn, class SuccFn>
class IterateAdaptor : public detail::Adaptor<IterateAdaptor<T,SuccFn,CondFn>>
{
    T initial;
    CondFn cond;
    SuccFn succ;

public:
    IterateAdaptor(const T &initial, const CondFn &cond, const SuccFn &succ) :
        initial(initial), cond(cond), succ(succ)
    {}

    //! The adaptor can be used as a regular range by introducing this iterator class.
    class const_iterator {
        T curr;
        const CondFn &cond;
        const SuccFn &succ;
        int counter;
        bool end;

    public:
        const_iterator(T initial, const CondFn &cond, const SuccFn &succ, int counter = -1) :
            curr(initial), cond(cond), succ(succ), counter(counter), end(counter == -1)
        {}
        const_iterator & operator=(const const_iterator &o) {
            curr = o.curr; counter = o.counter; end = o.end; return *this;
        }

        const T & operator*() const { return curr; }
        const T * operator->() const { return &curr; }
        const_iterator & operator++() { if (!end) { ++counter; curr = call(succ, curr); end = !bool{cond(curr)}; } return *this; }
        const_iterator operator++(int) { const_iterator old(*this); ++(*this); return old; }
        bool operator==(const const_iterator &o) const { return (end && o.end) || (counter == o.counter); }
        bool operator!=(const const_iterator &o) const { return !(*this == o); }

        // iterator traits
        using iterator_category = std::forward_iterator_tag;
        using value_type        = typename std::decay<decltype(*std::declval<const_iterator>())>::type;
        using difference_type   = std::size_t;
        using pointer           = const value_type*;
        using reference         = const value_type&;
    };

    const_iterator begin() const { return const_iterator(initial, cond, succ, 0); }
    const_iterator end()   const { return const_iterator(initial, cond, succ); }

    using iterator = const_iterator;
    using value_type = typename const_iterator::value_type;
};


}

#endif
