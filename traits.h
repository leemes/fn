#ifndef FN_TRAITS_H
#define FN_TRAITS_H

#include "call.h"

#include <algorithm>
#include <tuple>

namespace fn {

using std::begin;
using std::end;

/** Type traits for any type of (non-associative) range supported by fn. */
template<class R>
struct range_basic_type_traits {
    // "iterator": The type of the (const) iterators of a range
    using iterator = typename std::decay<decltype(begin(std::declval<R const>()))>::type;

    // "value_type": The (non-reference, non-const) type of the values of a range
    using value_type = typename std::decay<decltype(*(std::declval<iterator>()))>::type;
};

//! Conditionally define a "key_type" if supported by the range. In "range_type_traits" this gets merged into a final type traits class.
template<typename R, typename dummy = int> struct maybe_key_type {
    using has_key_value_iterator = std::false_type;
    // no "key_type" defined.
};
template<typename R> struct maybe_key_type<R, decltype(std::declval<typename range_basic_type_traits<R>::iterator const>().key(), 0)> {
    using has_key_value_iterator = std::true_type;
    // Add support for "key_type", since the range supports "key()" on its iterator type.
    using key_type = typename std::remove_reference<decltype(std::declval<typename range_basic_type_traits<R>::iterator const>().key())>::type;
};

//! Type traits for any type of range supported by fn.
template<class R>
struct range_type_traits : public range_basic_type_traits<R>, public maybe_key_type<R> {
};

//! Type alias templates to get a type from the type traits
template<class R>
using Iterator  = typename range_type_traits<R>::iterator;
template<class R>
using ValueType = typename range_type_traits<R>::value_type;
template<class R>
using KeyType   = typename range_type_traits<R>::key_type;




// Now we are adding more traits which can request the existance of a member function on a range...
// For the general approach on how this is done, see:  http://stackoverflow.com/a/12654277/592323



// These macros define a type with which a type (TARGET) can be checked for the existance of a member function. Use "TAGET" when specifying types for arguments (T1, T2, ...).
#define FN_DECLARE_TEST_HAS_MEM_FUNC_0_ARG(name, func) \
    struct _test_##name { \
        template<class TARGET> static auto test(TARGET* p) -> decltype(p->func(), std::true_type()); \
        template<class>        static auto test(...) -> std::false_type; \
    }; \
    template<class TARGET> using name = decltype(_test_##name::test<TARGET>(0));

#define FN_DECLARE_TEST_HAS_MEM_FUNC_1_ARG(name, func, T1) \
    struct _test_##name { \
        template<class TARGET> static auto test(TARGET* p) -> decltype(p->func(std::declval<T1>()), std::true_type()); \
        template<class>        static auto test(...) -> std::false_type; \
    }; \
    template<class TARGET> using name = decltype(_test_##name::test<TARGET>(0));

#define FN_DECLARE_TEST_HAS_MEM_FUNC_2_ARG(name, func, T1, T2) \
    struct _test_##name { \
        template<class TARGET> static auto test(TARGET* p) -> decltype(p->func(std::declval<T1>(), std::declval<T2>()), std::true_type()); \
        template<class>        static auto test(...) -> std::false_type; \
    }; \
    template<class TARGET> using name = decltype(_test_##name::test<TARGET>(0));


// Making some checkers we need later
FN_DECLARE_TEST_HAS_MEM_FUNC_0_ARG(has_key,              key)
FN_DECLARE_TEST_HAS_MEM_FUNC_1_ARG(has_push_back,        push_back, ValueType<TARGET>)
FN_DECLARE_TEST_HAS_MEM_FUNC_1_ARG(has_insert,           insert,    ValueType<TARGET>)
FN_DECLARE_TEST_HAS_MEM_FUNC_1_ARG(has_append,           append,    ValueType<TARGET>)
FN_DECLARE_TEST_HAS_MEM_FUNC_2_ARG(has_key_value_insert, insert,    KeyType<TARGET>, ValueType<TARGET>)
FN_DECLARE_TEST_HAS_MEM_FUNC_0_ARG(has_size,             size)
FN_DECLARE_TEST_HAS_MEM_FUNC_0_ARG(has_count,            count)


// Make a checker "callable<T, Args...>" which checks if a functor can be called with the provided arguments (however, the return type is ignored)
struct _test_callable_with {
    template<class TARGET, class ...Args> static auto test(TARGET* p) -> decltype(call(*p, std::declval<Args>()...), std::true_type());
    template<class...>                    static auto test(...) -> std::false_type;
};
template<class TARGET, class ...Args> using callable_with = decltype(_test_callable_with::test<TARGET,Args...>(0));


// Making the "enable_if" syntax a little bit more readable
template<bool Enable, typename Type>
using EnableIf = typename std::enable_if<Enable, Type>::type;



//! Depending on the checkers above, this function chooses the correct implementation to insert an element into the range of type R.
namespace detail
{
    template<class R, class T> inline
        EnableIf<has_push_back<R>::value,
    R&> do_insert(R &range, const T &v) {
        range.push_back(v);
        return range;
    }
    template<class R, class T> inline
        EnableIf<!has_push_back<R>::value && has_insert<R>::value,
    R&> do_insert(R &range, const T &v) {
        range.insert(v);
        return range;
    }
    template<class R, class T> inline
        EnableIf<!has_push_back<R>::value && !has_insert<R>::value && has_append<R>::value,
    R&> do_insert(R &range, const T &v) {
        range.append(v);
        return range;
    }
    // Support inserting pair types into Qt's associative containers having a member function "insert(const K &, const T &)"
    template<class R, class PairT> inline
        EnableIf<!has_push_back<R>::value && !has_insert<R>::value && !has_append<R>::value && has_key_value_insert<R>::value,
    R&> do_insert(R &range, const PairT &v) {
        // T is a pair type (std::pair, QPair, ...).
        // Split this pair "v" into key and value:
        range.insert(v.first, v.second);
        return range;
    }


    template<class R> inline
        EnableIf<has_size<R>::value,
    std::size_t> do_size(R &range) {
        return range.size();
    }
    template<class R> inline
        EnableIf<!has_size<R>::value && has_count<R>::value,
    std::size_t> do_size(R &range) {
        return range.count();
    }
    template<class R> inline
        EnableIf<!has_size<R>::value && !has_count<R>::value,
    std::size_t> do_size(R &range) {
        return std::distance(begin(range), end(range));
    }
}


//! Wrap the type traits as well as all SFINAE functions into one single templated traits class.
template<class R>
struct range_traits : public range_type_traits<R> {
    template<class T>
    inline static R & insert(R &range, const T &v) {
        // *** NOTE ***
        // If you see the error "No matching function for call to 'do_insert(...)'" here:
        //  => fn doesn't support inserting elements into the range of type "R".
        return detail::do_insert(range, v);
    }
    template<class K, class V>
    inline static R & insert(R &range, const std::tuple<K,V> &tuple) {
        // *** NOTE ***
        // If you see the error "No matching function for call to 'do_insert(...)'" here:
        //  => fn doesn't support inserting elements into the range of type "R".
        return detail::do_insert(range, std::make_pair(std::get<0>(tuple), std::get<1>(tuple)));
    }

    inline static std::size_t size(const R &range) {
        // *** NOTE ***
        // If you see the error "No matching function for call to 'do_size(...)'" here:
        //  => fn doesn't support counting the number of elements in the range of type "R".
        return detail::do_size(range);
    }
};


}

#endif // TRAITS_H
