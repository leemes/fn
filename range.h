#ifndef FN_RANGE_H
#define FN_RANGE_H

#include <iterator>
#include <cassert>
#include "traits.h"

namespace fn {


/**
 * Wrapper for an iterator pair.
 * Note that all functions working on ranges are templated and take any class
 * with an iterator interface like STL containers do. This class serves as a
 * wrapper to work with sub-sequences.
 */
template<class It>
class Range
{
    It b, e;

public:
    using const_iterator = It;
    using iterator = const_iterator;
    using value_type = typename std::iterator_traits<const_iterator>::value_type;

    inline Range(It begin, It end) : b(begin), e(end) {}

    inline It begin() const                           { return b; }
    inline It end() const                             { return e; }
    inline It cbegin() const                          { return b; }
    inline It cend() const                            { return e; }
    inline std::reverse_iterator<It> rbegin() const   { return std::reverse_iterator<It>(e); }
    inline std::reverse_iterator<It> rend() const     { return std::reverse_iterator<It>(b); }
    inline std::reverse_iterator<It> crbegin() const  { return std::reverse_iterator<It>(e); }
    inline std::reverse_iterator<It> crend() const    { return std::reverse_iterator<It>(b); }

    Range<std::reverse_iterator<It>> reversed() const {
        return { crbegin(), crend() };
    }
};


template<class R, class It = Iterator<R>>
class RangeCopy
{
    R range;

public:
    inline RangeCopy(const R &range) : range(range) {}

    inline It begin() const                          { return { std::begin(range) }; }
    inline It end() const                            { return { std::end(range) }; }
    inline It cbegin() const                         { return { std::begin(range) }; }
    inline It cend() const                           { return { std::end(range) }; }
    inline std::reverse_iterator<It> rbegin() const  { return std::reverse_iterator<It>(end()); }
    inline std::reverse_iterator<It> rend() const    { return std::reverse_iterator<It>(begin()); }
    inline std::reverse_iterator<It> crbegin() const { return std::reverse_iterator<It>(end()); }
    inline std::reverse_iterator<It> crend() const   { return std::reverse_iterator<It>(begin()); }

    RangeCopy<R, std::reverse_iterator<It>> reversed() const {
        return { crbegin(), crend() };
    }
};

/**
 * Wrapper for an iterator which container uses a key-value-interface (like QMap).
 * The wrapper exposes a pair-based interface (like std::map).
 *
 * The pair type used here is std::pair. Note that the reverse conversion
 * (converting a pair-style range in a key-value-interface like QMap) is supported
 * also for other pair types which expose their members through "first" and "second".
 */
template<class It, class Pair>
class key_value_to_pair_iterator {
public:
    // iterator traits
    using iterator_category = typename std::iterator_traits<It>::iterator_category;
    using value_type        = const Pair;
    using difference_type   = std::size_t;
    using pointer           = value_type*;
    using reference         = value_type&;

    inline key_value_to_pair_iterator(It original) : original(original) {}
    inline Pair operator *() const                                      { return Pair(original.key(), *original); }
    inline Pair*operator->() const                                      { return Pair(original.key(), *original); }
    inline key_value_to_pair_iterator& operator ++()                    { ++original; return *this; }
    inline key_value_to_pair_iterator operator ++(int)                  { key_value_to_pair_iterator old(*this); ++(*this); return old; }
    inline bool operator ==(const key_value_to_pair_iterator &o) const  { return original == o.original; }
    inline bool operator !=(const key_value_to_pair_iterator &o) const  { return original != o.original; }

private:
    It original;
};


/** Constructing a Range object from two iterators (begin and end). This is a type-deducing helper function for the constructor of Range<It>. */
template<class It>
inline Range<It> makeRange(It first, It last) {
    return Range<It>(first, last);
}
/** Constructing a Range object from an object which is already a Range object: Don't do anything. */
template<class It>
inline Range<It> makeRange(const Range<It> &range) {
    return range;
}
/** Constructing a Range object from an rvalue reference will copy the referenced object. */
template<class R,
         class It = Iterator<R>> inline
    EnableIf<!std::is_lvalue_reference<R>::value && !range_traits<R>::has_key_value_iterator::value,
RangeCopy<R,It>> makeRange(R &&range) {
    return range;
}
template<template<class,class> class PairType = std::pair,
         class R,
         class It = Iterator<R>,
         class Pair = PairType<KeyType<R>,ValueType<R>>,
         class PairIt = key_value_to_pair_iterator<It,Pair>> inline
    EnableIf<!std::is_lvalue_reference<R>::value && range_traits<R>::has_key_value_iterator::value,
RangeCopy<R,PairIt>> makeRange(R &&range) {
    return range;
}
template<class R,
         class It = Iterator<R>> inline
    EnableIf<!range_traits<R>::has_key_value_iterator::value,
Range<It>> makeRange(const R &range) {
    return {std::begin(range), std::end(range)};
}
template<template<class,class> class PairType = std::pair,
         class R,
         class It = Iterator<R>,
         class Pair = PairType<KeyType<R>,ValueType<R>>,
         class PairIt = key_value_to_pair_iterator<It,Pair>> inline
    EnableIf<range_traits<R>::has_key_value_iterator::value,
Range<PairIt>> makeRange(const R &range) {
    return {std::begin(range), std::end(range)};
}
/** Constructing a Range object from a literal C-string. The terminating null char is NOT included. */
template<std::size_t N>
inline Range<const char*> makeRange(const char (&cstring)[N]) {
    return Range<const char*>(&cstring[0], &cstring[N - 1]);
}
/** Type alias template for retrieving the type returned from "makeRange" when passed the given range to it. */
template<class R>
using MakeRange = decltype(makeRange(std::declval<R>()));







/** Returns the number of elements in the range. Alias: count, length. */
template<class R>
std::size_t size(const R &range) {
    return range_traits<R>::size(range);
}
template<class R>
std::size_t count(const R &range) {
    return range_traits<R>::size(range);
}
template<class R>
std::size_t length(const R &range) {
    return range_traits<R>::size(range);
}

/** Returns true iif the range is empty, i.e. if the begin iterator equals the end iterator. */
template<class R>
bool empty(const R &range) {
    return begin(range) == end(range);
}

/** Returns the first element of a range. */
template<class R, class T = ValueType<R>>
T head(const R &range) {
    return *std::begin(range);
}

/** Returns a modified pair of iterators where the first element will be skipped. */
template<class R, class It = Iterator<R>>
Range<It> tail(const R &range) {
    assert(!empty(range)); // the provided range has to be non-empty!
    auto first = begin(range);
    return Range<It>(++first, end(range));
}

/** Returns a modified pair of iterators where the last element will be skipped. */
template<class R, class It = Iterator<R>>
Range<It> skipLast(const R &range) {
    assert(!empty(range)); // the provided range has to be non-empty!
    auto last = end(range);
    return Range<It>(begin(range), --last);
}

/** Returns a modified pair of iterators with only (at most) the first N elements. */
template<class R, class It = Iterator<R>>
Range<It> take(const R &range, std::size_t n) {
    auto first = begin(range);
    std::advance(first, n);
    return Range<It>(begin(range), first);
}

/** Reversing a Range object or any type of container supported by fn. */
template<class R>
inline auto reverse(R &&range) -> decltype(makeRange(std::forward<R>(range)).reversed()) {
    return makeRange(std::forward<R>(range)).reversed();
}

/** Insert an element into a container. This automatically calls an appropriate member function on the container, depending on the type (one of: `push_back`, `append`, `insert`). */
template<class Container, class T>
Container & insert(Container & container, const T &v) {
    range_traits<Container>::insert(container, v);
    return container;
}

/** Insert all elements of a range into a container. The container is initially *not* cleared. */
template<class Container, class R>
void into(Container & container, R &&range) {
    // *** NOTE ***
    // If you see the error "'void v' has incomplete type" here:
    //  => Did you forget a 'return' statement in a lambda in fn::map?
    for (auto v : range)
        insert(container, v);
}

/** Convert any range to a container. Useful for converting between different container types in a single line of code. Automatically called when assigning any fn adaptor to a container. */
template<class Container, class R>
Container convert(R &&range, const Container &initial = Container()) {
    Container container = initial;
    into(container, range);
    return container;
}

}

#endif // FN_RANGE_H
