#ifndef FN_CALL_H
#define FN_CALL_H

namespace fn {

// Wrapper for a function call with less strict syntactic requirements than usual function calls.
// Uses further parameters as the function's first parameter (call(f,...) == f(...)).
// Supports calling member function/data pointers with the same syntax than normal functions.
// Supports the first argument to be a pointer instead of an object, also supporting any kind of smart pointers.
// With SFINAE, the correct overload is selected "automagically".



// Normal function call
template <class Fn, class ...Args>
auto call(Fn &&f, Args && ...args) -> decltype(f(std::forward<Args>(args)...)) {
    return f(std::forward<Args>(args)...);
}


// Member function call on instance
template <class T, class Ret, class ...Args, class ...ProvidedArgs>
auto call(Ret (T::*memFn)(Args...)const, const T &x, ProvidedArgs && ...args) -> decltype((x.*memFn)(std::forward<ProvidedArgs>(args)...)) {
    return (x.*memFn)(std::forward<ProvidedArgs>(args)...);
}
// Member function call on (dereferenced) pointer
template <class T, class Ret, class PtrT, class ...Args, class ...ProvidedArgs>
auto call(Ret (T::*memFn)(Args...)const, const PtrT &x, ProvidedArgs && ...args) -> decltype(((*x).*memFn)(std::forward<ProvidedArgs>(args)...)) {
    return ((*x).*memFn)(std::forward<ProvidedArgs>(args)...);
}


// Member data on instance
template <class T, class Ret>
auto call(Ret (T::*memVar), const T &x) -> decltype(x.*memVar) {
    return x.*memVar;
}
// Member data on (dereferenced) pointer
template <class T, class Ret, class PtrT>
auto call(Ret (T::*memVar), const PtrT &x) -> decltype((*x).*memVar)  {
    return (*x).*memVar;
}


}

#endif // FN_CALL_H
