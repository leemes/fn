#ifndef FN_TEMPLATED_FUNCTOR_H
#define FN_TEMPLATED_FUNCTOR_H


// "Templated functors" are a replacement for generic lambdas as introduced in C++14, of which we don't make use to stay compatible with C++11.


// The macro  FN_TEMPLATED_FUNCTOR_BIND_X_CALL_Y  defines a functor class binding X arguments in its constructor and having a templated call operator with Y arguments

#define FN_TEMPLATED_FUNCTOR_BIND_0_CALL_1(name, expr) \
    struct name { \
        explicit constexpr name() {} \
        template<class C1> \
        auto operator()(C1 &&c1) const -> decltype(expr) { \
            (void)c1; \
            return expr; \
        } \
    };
#define FN_TEMPLATED_FUNCTOR_BIND_1_CALL_1(name, expr) \
    template<class B1> struct name { \
        const B1 b1; \
        explicit constexpr name(const B1 &b1) : b1(b1) {} \
        template<class C1> \
        auto operator()(C1 &&c1) const -> decltype(expr) { \
            (void)c1; \
            return expr; \
        } \
    };



// Special useful functors

namespace fn {

// Accessing first / second of a pair generically
FN_TEMPLATED_FUNCTOR_BIND_0_CALL_1(Functor_getKey, c1.first)
static const constexpr Functor_getKey getKey;
FN_TEMPLATED_FUNCTOR_BIND_0_CALL_1(Functor_getValue, c1.second)
static const constexpr Functor_getValue getValue;

// Swapping the components of a pair
FN_TEMPLATED_FUNCTOR_BIND_0_CALL_1(Functor_swapKeyValue, std::make_pair(c1.second, c1.first))
static const constexpr Functor_swapKeyValue swapKeyValue;

}


#endif // FN_TEMPLATED_FUNCTOR_H
